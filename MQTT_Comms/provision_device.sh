#!/bin/bash

#comment out the below line if root CA vertificate is already created and registered
#source generateRootCert.sh

country=IR
state=Sligo
locality=Sligo
organization=Paul_Mulligan
organizationalunit=IoT
commonname=paul_mulligan
email=paul.mulligan@resourcekraft.com

#Creating a new AWS IoT thing based on user input name
echo "Enter name of new device to be provisoned : "
read deviceName

aws iot create-thing --thing-name $deviceName

echo "---------------------------------------"
echo "-----Generating Device certificate-----"
echo "---------------------------------------"
echo ""

openssl genrsa -out deviceCert.key 2048
openssl req -new -key deviceCert.key -out deviceCert.csr \
    -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

openssl x509 -req -in deviceCert.csr -CA myiotCAcert.pem -CAkey myCAprivatekey.key -CAcreateserial -out deviceCert.crt -days 500

echo "---- Registering device certificate --------"
echo ""

cert_info=$(aws iot register-certificate --certificate-pem file://deviceCert.crt --ca-certificate-pem file://myiotCAcert.pem --set-as-active)

echo 

cert_ARN=$(echo $cert_info | cut -c22- | rev | cut -c89- | rev)

echo $cert_info
echo ""
echo ""

echo "-----Attaching device certificate to device : $deviceName ------"
echo 

aws iot attach-thing-principal --thing-name $deviceName --principal $cert_ARN

echo "------Creating Policy called IoTAllAccess---------"
echo

aws iot create-policy --policy-name IoTSpecificAccess --policy-document file://policy_specific.json

echo "------Attaching Policy to device certificate------"
echo

aws iot attach-principal-policy --policy-name IoTSpecificAccess --principal $cert_ARN







