#!/bin/bash

country=CZ
state=Prague
locality=Prague
organization=ResourceKraft
organizationalunit=IoT
commonname=paul_mulligan
email=paul.mulligan@resourcekraft.com


#Get AWS registration code
reg_info=$(aws iot get-registration-code)
if [[ $reg_info == *"error"* ]]; then
  echo "error getting registraion code!"
else
reg_code=$(echo $reg_info | cut -c24- | rev | cut -c4- | rev)
echo $reg_code
fi

#Create Root CA certificate
echo "----------------------------------------"
echo "-----Generating Root CA Certificate-----"
echo "----------------------------------------"

openssl genrsa -out myCAprivatekey.key 2048
openssl req -x509 -new -nodes -key myCAprivatekey.key -sha256 -days 1024 -out myiotCAcert.pem \
    -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"


echo "generating verification certificate for Root CA certificate..."
openssl genrsa -out verificationCert.key 2048
openssl req -new -key verificationCert.key -out verificationCert.csr \
    -subj "/C=CZ/ST= /L= /O= /OU= /CN=$reg_code/emailAddress=paul@paul.com"
openssl x509 -req -in verificationCert.csr -CA myiotCAcert.pem -CAkey myCAprivatekey.key -CAcreateserial -out verificationCert.crt -days 500 -sha256
aws iot register-ca-certificate --ca-certificate file://myiotCAcert.pem --verification-cert file://verificationCert.crt --set-as-active



