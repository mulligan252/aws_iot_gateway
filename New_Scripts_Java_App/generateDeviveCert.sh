#!/bin/sh

country=IR
state=Sligo
locality=Sligo
organization=Paul_Mulligan
organizationalunit=IoT
commonname=paul_mulligan
email=paul.mulligan@resourcekraft.com

#Name of the device certificate and private key
deviceCert="deviceCert.crt"
deviceKey="deviceCert.key"

echo "`date`  : " >> provision_device.log

if [ -f $deviceCert ] && [ -f $deviceKey ]; then
echo "Device certificate and key is already created for this device. Skipping this step" | tee -a provision_device.log
exit 1
fi

echo "---------------------------------------"
echo "-----Generating Device certificate-----" | tee -a provision_device.log
echo "---------------------------------------"
echo ""

openssl genrsa -out deviceCert.key 2048
openssl req -new -key deviceCert.key -out deviceCert.csr -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

openssl x509 -req -in deviceCert.csr -CA CAcert.pem -CAkey CAprivatekey.key -CAcreateserial -out deviceCert.crt -days 500

chmod 777 deviceCert.csr deviceCert.key deviceCert.crt

echo "---------------------------------------"
echo "-----Device certificate generated-----" | tee -a provision_device.log
echo "---------------------------------------"
echo ""

