#!/bin/bash

#comment out the below line if root CA vertificate is already created and registered
#source generateRootCert.sh

country=IR
state=Sligo
locality=Sligo
organization=Paul_Mulligan
organizationalunit=IoT
commonname=paul_mulligan
email=paul.mulligan@resourcekraft.com

#Name of the device certificate and private key
deviceCert="deviceCert.crt"
deviceKey="deviceCert.key"

#Reading the device serial number from the EEPROM which will be used as the device name
deviceName="LM_New_"$(hexdump -e '8/1 "%c"' /sys/bus/i2c/devices/0-0050/eeprom -s 16 -n 12)

echo "`date`  : " >> provision_device.log

#If creating a new AWS IoT thing based on user input name (For testing purposes) 
#echo "Enter name of new device to be provisoned : "
#read deviceName

if [ -f $deviceCert ] && [ -f $deviceKey ]; then
echo "Device certificate and key is already created for this device : $deviceName . Device will not be provisioned" | tee -a provision_device.log
exit 1
fi


echo "Creating device with name $deviceName"

aws iot create-thing --thing-name $deviceName

echo "---------------------------------------"
echo "-----Generating Device certificate-----" | tee -a provision_device.log
echo "---------------------------------------"
echo ""

openssl genrsa -out deviceCert.key 2048
openssl req -new -key deviceCert.key -out deviceCert.csr -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

openssl x509 -req -in deviceCert.csr -CA myiotCAcert.pem -CAkey myCAprivatekey.key -CAcreateserial -out deviceCert.crt -days 32786

echo "---- Registering device certificate --------"
echo ""

cert_info=$(aws iot register-certificate --certificate-pem file://deviceCert.crt --ca-certificate-pem file://myiotCAcert.pem --set-as-active)

cert_ARN=$(echo $cert_info | jq -r '.certificateArn')

echo "" | tee -a provision_device.log
echo "cert_info" | tee -a provision_device.log
echo $cert_info | tee -a provision_device.log
echo "" | tee -a provision_device.log

#This below parse must be modified later, because the cert_info string is not always the same
#cert_ARN=$(echo $cert_info | cut -c21- | rev | cut -c89- | rev)

#Get index of where arn:aws is inside cert_info string. This is needed to extract the ARN string in next step - Not Used currently
idx_begin=$(echo $cert_info | grep -b -o "arn:aws" | awk 'BEGIN {FS=":"}{print $1}')


echo "cert_ARN" | tee -a provision_device.log
echo $cert_ARN | tee -a provision_device.log
echo "" | tee -a provision_device.log

echo "-----Attaching device certificate to device : $deviceName ------" | tee -a provision_device.log
echo "" | tee -a provision_device.log

aws iot attach-thing-principal --thing-name $deviceName --principal $cert_ARN | tee -a provision_device.log

echo "------Creating Policy called IoTSpecificAccess---------" | tee -a provision_device.log
echo "" | tee -a provision_device.log

#Below line should be uncommented if we are creating and adding a new policy
#aws iot create-policy --policy-name IoTSpecificAccess --policy-document file://policy_specific.json

echo "------Attaching Policy to device certificate------" | tee -a provision_device.log
echo "" | tee -a provision_device.log

aws iot attach-principal-policy --policy-name IoTSpecificAccess --principal $cert_ARN | tee -a provision_device.log
echo " " >> provision_device.log





