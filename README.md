# AWS_IoT_Gateway

Paul Mulligan, 11.12.2018


Scripts for the device / gateway side used to communicate with the AWS IoT cloud platform.


We will use the JITP (Just_in-Time_Provisioning) feature of Amazon AWS IoT. 
This means that the device gateways will generate their own self-signed device certificates and register them 
with other device information on the AWS IoT platform during provisioning.

In order to do this, the AWS IoT platform requires us to first create and register a root CA certificate on the 
AWS IoT platform which will be used to verify our individual device certificates.

The script to generate and register a root CA certificate is generateRootCert.sh. Prior to running this script,
the device must be already signed in with Amazon IAM user details with command "aws configure". This step will be automated later.

The main script for provisioning is provision_device.sh. 
If a root CA is already generated and registered, we can comment out line "source generateRootCert.sh" inside provision_device.sh.

On running the script provision_device.sh, it will ask for the name of the "thing" or device which we want to provision.
It will then create a thing/dvice on the Amazon AWS IoT platform, create and associate a unique device certificate and policy with it.

(The device certificate is signed using the root CA private key, and hence validated on the AWS IoT platform using the previosuly stored root CA certificate)


It is then possible for the device to communicate securely with the AWS Iot platform via MQTT after the device is provisioned in this manner. 
 